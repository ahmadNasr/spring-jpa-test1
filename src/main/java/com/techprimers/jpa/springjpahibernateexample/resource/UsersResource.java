package com.techprimers.jpa.springjpahibernateexample.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.techprimers.jpa.springjpahibernateexample.model.Users;
import com.techprimers.jpa.springjpahibernateexample.repository.UsersRepository;

@RestController
@RequestMapping("/rest/users")
public class UsersResource {

	@Autowired
	UsersRepository usersRepository;

	@RequestMapping("/hello")
	@ResponseBody
	public String pleaseSayHello() {
		return "Hello";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void addUser(@RequestBody Users user) {
		usersRepository.save(user);
	}

	@GetMapping("/all")
	public List<Users> getAll() {
		return usersRepository.findAll();
	}

	@GetMapping("/{name}")
	public List<Users> getUser(@PathVariable("name") final String name) {
		return usersRepository.findByName(name);

	}

	@GetMapping("/id/{id}")
	public Users getId(@PathVariable("id") final Integer id) {
		return usersRepository.findById(id).get();
	}

	@GetMapping("/update/{id}/{name}")
	public Users update(@PathVariable("id") final Integer id, @PathVariable("name") final String name) {

		Users users = getId(id);
		users.setName(name);

		return usersRepository.save(users);
	}
}
